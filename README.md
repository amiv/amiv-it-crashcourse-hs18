# AMIV IT Crashcourse

During this course, you'll learn the basics of coding together with
[git](https://git-scm.com), how to send requests to AMIV API using
[Postman](https://www.getpostman.com) and finally how to write a simple
[JavaScript](https://www.javascript.com) single-page application fetching and
displaying AMIV events using [mithril](https://mithril.js.org).

Presentations:

* [GIT](https://docs.google.com/presentation/d/11PCXIRaPWw4izQvDXTf1ZKJTBJMvDxi9S5JxWHviHkQ/edit?usp=sharing)
* [AMIV API](https://docs.google.com/presentation/d/101VYXbO7N0jcGytwSdYGyYx9KJCA4AavsAO4PsGYF2o/edit?usp=sharing)
* [javascript](https://docs.google.com/presentation/d/1t3Vnxry78rfEZawJD2v0wV6zpakhm3zfeGLo_P4NLVo/edit?usp=sharing)

## Task 1: Effective Collaboration with git

In your first task, you'll learn how to use git.

If you are using MacOS or Linux, you already have git installed.
For Windows, you can [download it here](https://git-scm.com/download/win).

### 1. Fork this repository

You do not have the permissions to change anything in this repository --
and using git, that's ok!

Your first task is to *fork* this repository, i.e. to create your own copy.
In the web-interface, look for the `Fork` button and click it!

[More info](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html).

### 2. Clone your repository

If everything went well, you should have left our repository and look at your
own version of it.

Now it's time to download it to your computer. Right next to the `Fork`
button you can see the repository url with a handy *copy to clipboard* button.
Copy that URL.

Next, open a terminal on your computer and enter the following command:

```sh
git clone <the url you just copied>
```

This will create a folder `amiv-it-crashcourse-hs18` with all the files in the
repository in it.

Enter the directory using:

```sh
cd amiv-it-crashcourse-hs18
```

And take a moment to look at some information git offers you:

- `git status` will show you an overview of current changes etc.
- `git log` Shows you the repository history
- `git log --pretty=oneline` provides a more compact view


### 3. Make changes

Time to do something! Open the `participants` directory and add a file wit
your name.

### 4. Add & Commit & Push

If you are happy with your work, it is time to *commit* your changes:
1. First, use the `git add` command to select which changes you want to commit,
    in this case `git add participants/your_file`
2. If you have added all changes, commit them using `git commit`.
   A text editor will open, asking you to describe your commit.
   For now, a single line such as `Add my name to participants list` will be
   enough, but commit messages are very important to keep track of your work.
   [Tips for useful commit messages](https://chris.beams.io/posts/git-commit/).  
   Pro-Tip: The standard editor in your terminal is usually vim. To exit the
   'insert-mode' of vim press `<ESC>`, then write `:wq` to write and quit the
   file.
3. You now have committed your changes, but this only affects your local files.
   It is now time to *push* your local work to the server with the `git push`
   command. 

### 5. Submit a *merge request*

After you have uploaded your changes to gitlab, you'll notice a new button pop
up in the web-interface:
It will ask you whether you want to submit a *Merge Request* -- this is how
your personal changes get back to the original repository!

If gitlab doesn't ask you to submit a merge request on your own, click on
`Merge Requests` in the menu on the left, and then `New Merge Request`.

Follow the on-screen instructions to submit a merge request.

Congratulations! You have contributed to your first project using git!

### Bonus: Set up a SSH-key

Follow [this guide](https://docs.gitlab.com/ee/ssh/) to set up SSH
authentication and you'll never have to enter a password again.

### Additional Resources

- [`Oh shit, git!' -- quick fixes to common problems](https://ohshitgit.com)
- [Tutorials by github.com](https://try.github.io)

## Task 2: Understanding REST requests

[REST](https://en.wikipedia.org/wiki/Representational_state_transfer) is mostly
a style for web services. While there are many different services, they all
follow the same principles, which makes them easy to interact with. The AMIV
API is an example of such a web service, and in this task you'll learn how to
access it.

First of all install [Postman](https://www.getpostman.com), a useful tool to
send requests to RESTful web services. (When starting postman, you are asked to
register. This is not necessary, you can skip this at the bottom of the screen).

Postman allows you interact with web services in many ways, but today we are
just going to send a *request*.

### 1. Discovering Resources

A RESTful service is a collection of *resources*, such as users, events, etc.,
which consist of *items*, i.e. a single user, event, etc.

You can interact with these resources in four different ways, which are
abbreviated with *CRUD*, and correspond to HTTP requests:

- **C**reate: Create new items on the server, using the HTTP method `POST`
  (or sometimes `PUT`)
- **R**ead: Access items/resources, using the method `GET`
- **U**pdate: Modify items using `PATCH` (or sometimes `PUT`)
- **D**elete: Remove items using `DELETE`

In this tutorial, we'll only be looking at information, so you will only need
the `GET` method.

To send a HTTP request, you need a method (`GET`) and an URL.

Send a `GET` request to `https://api.amiv.ethz.ch`. As a response, you will
receive an overview of all available resources.

### 2. Listing all Events

You have probably spotted the resource `events` in the list -- let's have a
closer look.

Send another `GET` request to `https://api.amiv.ethz.ch/events`. You'll receive
a list with plenty of info!

### 3. Inspecting a Particular Event

You can not only list all items of a resource, but access single items as well.
Take a look at any of the items you have received, you'll notice that it
contains an `_id` value -- this value is very important, it uniquely identifies
this item for the given resource
(so there can be no other event with the same id).

Copy the `_id` (without the quotation marks) and send another
`GET` request to `https://api.amiv.ethz.ch/events/<the_id_you_just_copied>`.
You should receive a response containing only this item.

Congratulations! You now know how to access the AMIV REST API.

## Task 3: Your first mithril.js project

[mithril](https://mithril.js.org/) is a javascript library that powers many of
AMIV's web projects. Beeing a very small library, mithril requires you to
learn standard javascript with very little extras, so everything you learn here
can be helpful for your future projects.

The very first step is the installation. We start by cloning the standard
javascript boilerplate for AMIV projects. This repository sets up an empty
'node.js module' with [webpack](https://webpack.js.org/) and 
[eslint](https://eslint.org/). Too many new names? Let's clone the repository
and get an overview:

```sh
git clone git@gitlab.ethz.ch:amiv/amiv-js-boilerplate.git <your project name>
cd <your project name>
```

Now, in the current directory, you will find the following structure:
```
src/
.eslintrc.js
package.json
webpack.config.js
<lots of other stuff we ignore>
```

`package.json` defines this directory as the root of a node module. In the file,
you will see a list of dependencies to other modules that need to be installed.
In order to use the module and set up all the dependencies, first make sure that
you have node and the 'node package manager' installed by trying:
```sh
npm --version
```
If `npm` was not found, please make sure to 
[install node](https://nodejs.org/en/download/package-manager/).

Once node is set up, we can install all the initial dependencies:
```sh
npm install
```

This will now install e.g. `webpack` and `eslint`. What do they do? `webpack` or
similar software makes it possible to organize our projects into different
files. As you are used to from other coding languages, `webpack` enables us to
`import <func> from './path/to/file';` in our code. Yes, this is not a default
function in javascript as javascript is designed to run in a browser, that
usually does not want to downlaod 100 different files. So webpack will later
automatically bundle all our code together into 1 unreadable file that can be
read by the browser. Let's not worry too much about it now, take-away message is
that webpack is helpful.  
`eslint` is completely irrelevant for the code itself. It is a 'linter' and it
helps keeping our code clean. You can activate the linter either through
`eslint ./` in your terminal or with some addon to your editor. It will then
show you errors in your code both regarding functionallity and style. This helps
us to keep the code understandable to everyone involved in AMIV's projects. Many
projects check for linter-errors before any code is merged into their 
repositories.

### 1. Set up mithril
With the standard boilerplate running, we can now install mithril:

```sh
npm install --save mithril
```

Note that the `--save` option automatically adds mithril as a requirement to
your `package.json`, so anybody who will in the future set up the project does not
have to install it manually.

We also clean the source directory in the boilerplate:
```sh
rm src/*
```

Now we can create our first file, `src/index.js` and with a simple mithril
hello world example:

```js
import m from 'mithril';

const root = document.body;

m.render(root, m('h1', 'Hello World!'));
```

Save the file, open another terminal window with the same working directory,
and start the development server with `npm start`. You can now either click on
the link in the terminal or open your browser manually at `localhost:9000`. If
everything is working, you should be greeted with 'Hello World!'.  
To harvest the sweet fruit of all the work above, go ahead and change the text
in `src/index.js`. You will see that the browser window automatically refreshes
with any changes you are making to the code.

### 2. Dynamic Content from the AMIV API
You already learned how to interact with the AMIV API and how to display content
with mithril. We will now bring everything together by displaying events from
the API in your custom web ui.

As dynamic content can change, we need to represent some kind of state in our
javascript app that can then be updates by requests to the API. To do this, we
create a small class:

```js
class EventList {
  constructor() {
    // events should have a well-defined default state
    this.events = [];
  }
  
  oninit() {
    // load the events using m.request
    // < fill in your code here>
  }
  
  view() {
    return m('div', [
      m('h1', 'title'),
      // < insert list of events >
    ]);
  }
}

m.mount(root, EventList);
```
The last line shows how mithril and javascript classes work together: You can
group logical content together into a class and then render them in the same way
as html elements.  
Any class object that is rendered with mithril has a `view()` function that is
called when the element is shown, and has to return some mithril rendered html
content. It also has an `oninit()` function that is called when the element is
created for the first time. You can learn more about mithril components 
[here](https://mithril.js.org/components.html).    
The state of the `EventList` is simply tracked in `this.events`, which is
available throughout the lifetime of the object.

To fill in the code loading the events, have a look at the documentation of
[`m.request`](https://mithril.js.org/request.html), a small function in mithril
that can do requests to REST APIs.